const hapi = require( '@hapi/hapi' );
const nodemailer = require ( 'nodemailer' );
const jwt = require ( 'njwt' );
const { MongoClient, ObjectId } = require( 'mongodb' );
const Joi = require( 'joi' );

const secretKey = 'userApisecretkey7';
const conString =
    'mongodb://localhost:27017/?readPreference=primary' + 
    '&appname=MongoDB%20Compass&directConnection=true&ssl=false';
const client = new MongoClient( conString );

function getCollection( dbName, collectionName ) {
    client.connect();
    const db = client.db( dbName );
    return db.collection( collectionName );
}

const mailService = nodemailer.createTransport( {
    service: "Gmail",
    auth: {
        user: "psindustriesofficial@gmail.com",
        pass: "PKindustries37$"
    },
} );

const verifyLink = 'http://localhost:7777/verify';

const server = hapi.server( {
    port: 7777,
    host: 'localhost'
} );

const alphabetOnlyPattern = /^\D+$/;

server.route( {
    method: 'POST',
    path: '/',
    options: {
        validate: {
            payload: Joi.object( {
                firstName: 
                    Joi.string()
                    .pattern( alphabetOnlyPattern )
                    .min( 3 )
                    .max( 30 )
                    .required(),
                lastName:
                    Joi.string()
                    .pattern( alphabetOnlyPattern )
                    .min( 3 )
                    .max( 30 )
                    .required(),
                email: 
                    Joi.string()
                    .email( {
                        minDomainSegments: 2,
                        tlds: { allow: [ 'com', 'net' ] } 
                    } )
                    .required(),
                password: Joi.string().min( 8 ).required()
            } )
        }
    },
    handler: async ( req, h ) => {
        const SampleTable = getCollection( 'demo', 'users' );

        const result = await SampleTable.insertOne( {
            firstname: req.payload.firstName,
            lastname: req.payload.lastName,
            email: req.payload.email,
            password: req.payload.password,
            verified: false
        } );

        const link = verifyLink + `/${ result.insertedId.toString() }`;
        const mailOptions = {
            from: 'psindustriesofficial@gmail.com',
            to: req.payload.email,
            subject: 'Email Verification',
            text: `Please click the link ${ link } to verify your account `
        };
        await mailService.sendMail( mailOptions );
        return {
            message: 'User Registered Successfully!'
        };
    }
} );

server.route( {
    method: 'GET',
    path: '/verify/{id}',
    handler: async ( req, h ) => {
        const SampleTable = getCollection( 'demo', 'users' );
        await SampleTable.updateOne(
        { _id: ObjectId( req.params.id ) },
        { $set : { verified: true } } );
        return 'Your Account is Activated!';
    }
} );

server.route( {
    method: 'POST',
    path: '/login',
    options: {
        validate: {
            payload: Joi.object( {
                email: 
                    Joi.string()
                    .email( {
                        minDomainSegments: 2,
                        tlds: { allow: [ 'com', 'net' ] } 
                    } )
                    .required(),
                password: Joi.string().min( 8 ).required()
            } )
        }
    },
    handler: async ( req, h ) => {
        const SampleTable = getCollection( 'demo', 'users' );
        const result = await SampleTable.findOne( { 
            email: req.payload.email,
            password: req.payload.password
        } );
        if ( result && result.verified ) {
            const data = {
                id: result._id.toString(),
            };
            const token = jwt.create( data, secretKey );
            token.setExpiration( new Date().getTime() + 600 * 1000 )
            client.close();
            return { 
                message: 'Login Successfull!',
                token: token.compact() 
            };
        } else {
            const data = { message: 'Incorrect email or password' };
            return h.response( data ).code( 400 );
        }
    }
} );

server.route( {
    method: 'GET',
    path: '/{token}',
    handler: async ( req, h ) => {
        const token = req.params.token;
        const authResult = await jwt.verify( token, secretKey );
        const SampleTable = getCollection( 'demo', 'users' );
        const result = await SampleTable.findOne( { _id: ObjectId( authResult.body.id ) } );
        return {
            firstname: result.firstname,
            lastname: result.lastname,
            email: result.email
        };
    }
} );

server.start()
.then( () => {
    console.log( 'server is running' );
} )
.catch( ( err ) => {
    console.log( err );
} )

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});